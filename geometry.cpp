#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef complex<long double> point;

#define dot(a,b) ((conj(a)*(b)).real())
#define length(a) hypot((a).X,(a).Y)

#define X real()
#define Y imag()
#define vect(p1,p2) ((p2)-(p1))
#define cross(a,b) ((conj(a)*(b)).imag())
const long double EPS = 1e-12;
const long double INF = 1e18;

#define STRAIGHT 0
#define CLOCKWISE 1
#define COUNTERCLOCKWISE 2
int orient(const point &a, const point &b, const point &c){
    long double res = cross(vect(a, b), vect(a, c));

    if(res < -EPS)
        return CLOCKWISE;
    if(res > EPS)
        return COUNTERCLOCKWISE;

    return STRAIGHT;
}

struct cmp {
	point about;
	cmp(point about): about(about) {}

	bool operator()(const point& p, const point& q) const {
		int res = orient(p, about, q);

		if (res == STRAIGHT)
			return make_pair(p.Y, p.X) < make_pair(q.Y, q.X);
		return res == CLOCKWISE;
	}
};

vector<point> convex_hull(vector<point> points) {
    vector<point> convex;

	point mn(INF, INF);
	for (int i = 0; i < points.size(); i++) {
		if (make_pair(points[i].Y, points[i].X) < make_pair(mn.Y, mn.X))
			mn = points[i];
	}

	sort(points.begin(), points.end(), cmp(mn));

	convex.clear();
	convex.push_back(points[0]);

	if (points.size() == 1)
		return convex;

	convex.push_back(points[1]);

	for (int i = 2; i <= points.size(); i++) {
		point c = points[i % points.size()];

		while (convex.size() > 1) {
			point b = convex[convex.size() - 1];
			point a = convex[convex.size() - 2];

			if (orient(a,b,c) == COUNTERCLOCKWISE)
				break;

			convex.pop_back();
		}
		if (i < points.size())
			convex.push_back(points[i]);
	}

	return convex;
}

long double point_line_distance(const point& a, const point& b, const point& p) {
	return fabs(cross(b - a, p - a)) / length(b - a);
}
long double pointSegmentDistance(const point& a, const point& b, const point& p) {
	if (dot(p-a,b-a) < EPS)
		return length(p - a);
	if (dot(p-b,a-b) < EPS)
		return length(p - b);
	return point_line_distance(a, b, p);
}

bool point_on_line(const point &a, const point &b,const point &p) {
	return fabs(cross(vect(a,b), vect(a,p))) < EPS;
}

bool point_on_segment(const point &a,const point &b,const point &p) {
	return dot(vect(a,b), vect(a,p)) > -EPS && point_on_line(a, b, p)
			&& dot(vect(b,a), vect(b,p)) > -EPS;
}

struct orient_cmp {
	point a;
	orient_cmp(point a): a(a){}

	bool operator()(const point& c, const point& b) const {
	    return orient(a, b, c) == CLOCKWISE;
	}
};

bool point_in_triangle(const point& a, const point& b, const point& c, const point& p){
    long double s1 = fabs(cross(vect(a, b), vect(a, c)));
    long double s2 = fabs(cross(vect(p, a), vect(p, b))) + fabs(cross(vect(p, b), vect(p, c))) + fabs(cross(vect(p, c),vect(p, a)));

    return fabs(s1 - s2) < EPS;
}

bool point_in_convex_polygon(const vector<point> &convex, const point &p){
    int n = convex.size();

    if(orient(convex[0], convex[1], p) == CLOCKWISE)
        return false;

    if(orient(convex[0], convex[n-1], p) == COUNTERCLOCKWISE)
        return false;

    if(point_on_segment(convex[0], convex[1], p))
        return true;

    if(point_on_segment(convex[0], convex[n-1], p))
        return true;

    auto pos = lower_bound(convex.begin() + 1, convex.end(), p, orient_cmp(convex[0])) - convex.begin();

    if(pos == 1 || pos == n)
        return false;

    if(point_in_triangle(convex[0], convex[pos-1], convex[pos], p))
        return true;

    if(point_on_segment(convex[pos-1], convex[pos], p))
        return true;

    return false;
}
